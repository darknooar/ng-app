import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent         } from './app.component';
import { AboutComponent       } from './about/about.component';
import { InfoComponent        } from './info/info.component';
import { MainComponent        } from './main/main.component';
import { TestContentComponent } from './test-content/test-content.component';
import { TestComponent        } from './test-content/test.component';


@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    InfoComponent,
    MainComponent,
    TestContentComponent,
    TestComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
