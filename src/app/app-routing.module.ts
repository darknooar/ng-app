import { NgModule 				} from '@angular/core';
import { Routes, RouterModule 	} from '@angular/router';

import { MainComponent 			} from './main/main.component';
import { InfoComponent 			} from './info/info.component';
import { AboutComponent 		} from './about/about.component';
import { TestContentComponent 	} from './test-content/test-content.component';
import { TestComponent 			} from './test-content/test.component';

const routes: Routes = [
	{ path: 'main'	, pathMatch: 'full', redirectTo: 'main/qwe'},
	{ 
		path: 'main',
		component: MainComponent,
		children:[
	        {
	        	path: 'qwe',
	        	component: TestContentComponent 
	        },
	        {
	        	path: 'zxc',
	        	component: TestComponent
	        }
	    ]
	},
	{ path: 'about'	,  	component: AboutComponent 				},
	{ path: 'info'	, 	component: InfoComponent 				},
	{ path: ''		,	pathMatch: 'full', redirectTo: 'main/qwe' 	},
	{ path: '**'	,	pathMatch: 'full', redirectTo: 'main/qwe' 	},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
